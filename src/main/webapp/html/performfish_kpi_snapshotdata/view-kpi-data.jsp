<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@include file="../init.jsp"%>

<portlet:defineObjects />
<portlet:resourceURL var="resourceURL">
</portlet:resourceURL>
<%
Group group = (Group) GroupLocalServiceUtil.getGroup(PortalUtil.getScopeGroupId(request));
long currentGroupId = group.getGroupId();
pageContext.setAttribute("groupId", currentGroupId);

%>

<script type="text/javascript">
	function submitCallToPerformService(endpoint, groupId) {
		console.log("submitCallToPerformService:" + endpoint);
		$('#performServiceButton').attr("disabled", true);
		$('#performServiceButton')
				.html(
						'<i class="icon-refresh icon-white">in progress ...please wait');
		$('#feedbackResult').html('');
		$.ajax({
			url : endpoint,
			type : 'POST',
			datatype : 'json',
			data : {
				groupId : groupId,
				userId : Liferay.ThemeDisplay.getUserId()
			},
			success : function(data) {
				var resultArray = JSON.parse(JSON.stringify(data));
				var feedbackResultHTML = '<p class="lead">Operation Result</p><ul>';
				resultArray.forEach(item => {
					var feedbackVisual = item.success ? ' <span style="color: green;">created succesfully</span> ' : '  <span style="color: red;">failed</span> ';
					feedbackResultHTML += '<li>'+item.comment+':'+ feedbackVisual+'</li>';
				});
				feedbackResultHTML += '</ul>'
				$('#feedbackResult').html(feedbackResultHTML);
				$('#performServiceButton').hide();
				$('#reloadPageButton').show();
			}
		});
	}
</script>



<button name="snapshotdata" id="performServiceButton" type="button"
	class="btn btn-primary"
	onClick="submitCallToPerformService('${resourceURL}', '${groupId}')">Get
	New Data</button>
<button name="refresh" id="reloadPageButton" type="button"
	class="btn btn-primary" onClick="location.reload();">Please
	click here to refresh the content of the folder, or to enable the Get
	Data functionality again</button>

<div id="feedbackResult"></div>


<script>
$('#reloadPageButton').hide();
</script>
