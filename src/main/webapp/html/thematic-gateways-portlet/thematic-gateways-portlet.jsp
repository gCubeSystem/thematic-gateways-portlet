<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@include file="../init.jsp"%>
<%
pageContext.setAttribute("thematicGatewayIntro",
		GetterUtil.getString(portletPreferences.getValue("thematicGatewayIntro", StringPool.BLANK)));

String sitesIdToExclude = GetterUtil.getString(portletPreferences.getValue("sitesIdToExclude", StringPool.BLANK));

List<Long> groupIds2Exclude = new ArrayList<Long>();
if (sitesIdToExclude != null && sitesIdToExclude.compareTo("") != 0) {
	String[] groups2Exclude = sitesIdToExclude.split(",");
	for (int i = 0; i < groups2Exclude.length; i++) {
		groupIds2Exclude.add(Long.parseLong(groups2Exclude[i]));
	}
}

boolean sortByVRENumber = GetterUtil.getBoolean(portletPreferences.getValue("orderPerVRENumber", StringPool.FALSE));

List<Gateway> theGateways = (List<Gateway>) request.getAttribute("theGateways");
if (sortByVRENumber)
	Collections.sort(theGateways, new GatewayComparator());

Map<Group, ArrayList<String>> theGatewaysMap = new LinkedHashMap<Group, ArrayList<String>>();
List<Group> visibleGroups = new ArrayList<Group>();
//this for constructs the (perhaps sorted list to show and exclude the groupIds of the sites specieified in the config)
for (Gateway gat : theGateways) {
	if (!groupIds2Exclude.contains(gat.getSite().getGroupId())) {
		theGatewaysMap.put(gat.getSite(), gat.getVres());
		visibleGroups.add(gat.getSite());
	}
}

PortletURL portletURL = PortletURLFactoryUtil.create(request, portletDisplay.getId(), plid, PortletRequest.RENDER_PHASE);
%>
		<div class="lead">
		${thematicGatewayIntro}
		
		</div>
		<liferay-ui:search-container emptyResultsMessage="no-sites-were-found"	iteratorURL="<%= portletURL %>">
		
			<%
			total = visibleGroups.size();
			searchContainer.setTotal(total);			
			%>
		
			<liferay-ui:search-container-results
				results="<%= ListUtil.subList(visibleGroups, searchContainer.getStart(), searchContainer.getEnd()) %>" />
		
			<liferay-ui:search-container-row className="com.liferay.portal.model.Group" modelVar="childGroup">
			<%
			LayoutSet layoutSet = null;
			
			if (childGroup.hasPublicLayouts()) {
				layoutSet = childGroup.getPublicLayoutSet();
			}
			else {
				layoutSet = childGroup.getPrivateLayoutSet();
			}
			int vresNumber = theGatewaysMap.get(childGroup).size();
			String labelVRE = (vresNumber > 1) ? "VREs / VLabs" : "VRE / VLab";
			String theTitle = HtmlUtil.escape(childGroup.getDescriptiveName(locale)) + ", " + vresNumber + " " + labelVRE;
			String theDescription = HtmlUtil.escape(childGroup.getDescription());
			
			final long companyId = PortalUtil.getDefaultCompanyId();
			long layoutSetId = layoutSet.getLayoutSetId();
			String vHost = VirtualHostLocalServiceUtil.getVirtualHost(companyId, layoutSetId).getHostname();
			String theURL = "https://"+vHost+"/explore";
			%>
		
				<liferay-ui:app-view-entry
					assetCategoryClassName="<%= Group.class.getName() %>"
					assetCategoryClassPK="<%= childGroup.getGroupId() %>"
					assetTagClassName="<%= Group.class.getName() %>"
					assetTagClassPK="<%= childGroup.getGroupId() %>"
					description="<%=theDescription%>"
					displayStyle="descriptive" showCheckbox="<%= false %>"
					thumbnailSrc='<%= themeDisplay.getPathImage() + "/layout_set_logo?img_id=" + layoutSet.getLogoId() + "&t=" + WebServerServletTokenUtil.getToken(layoutSet.getLogoId()) %>'
					title="<%=theTitle %>"
					url="<%= theURL %>" />
					

			</liferay-ui:search-container-row>
		
			<liferay-ui:search-paginator searchContainer="<%= searchContainer %>" />
		</liferay-ui:search-container>
