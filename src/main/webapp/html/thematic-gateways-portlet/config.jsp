<%@include file="../init.jsp"%>

<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />

Here you can customise the text to show on top
<%
	String defaultText = "The following Infrastructure Gateways serve a number of Research Communities, each of which having a specific scientific domain and dedicated Virtual Research Environments / Virtual Laboratories.";
	String thematicGatewayIntro_cfg = GetterUtil
			.getString(portletPreferences.getValue("thematicGatewayIntro", defaultText));

	String sitesIdToExclude_cfg = GetterUtil
			.getString(portletPreferences.getValue("sitesIdToExclude", StringPool.BLANK));

	boolean orderPerVRENumber_cfg = GetterUtil
			.getBoolean(portletPreferences.getValue("orderPerVRENumber", StringPool.FALSE));
%>


<aui:form action="<%=configurationURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />
	<aui:input style="width: 100%;"
		name="preferences--thematicGatewayIntro--" type="text"
		cssClass="text long-field" showRequiredLabel="true"
		label="Enter the text you want to display" inlineField="false"
		inlineLabel="left" placeholder="thematic Gateway Introductory text"
		helpMessage="thematic Gateway Introductory text"
		value="<%=thematicGatewayIntro_cfg%>" required="false">
	</aui:input>
	<aui:input style="width: 100%;" name="preferences--sitesIdToExclude--"
		type="text" cssClass="text long-field" showRequiredLabel="true"
		label="Enter the Site ID of the Sites to exclude" inlineField="false"
		inlineLabel="left" placeholder="comma separated groupIds"
		helpMessage="Enter the Site ID of the Sites (gateway Site) you want to exclude, comma separated"
		value="<%=sitesIdToExclude_cfg%>" required="false">
	</aui:input>

	<aui:input 
		helpMessage="Check if you want to order the Gateways per number of VREs they contain (more on top)"
		label="Order the Gateways per number of VREs (Default Alphabetically)"
		name="preferences--orderPerVRENumber--" type="checkbox"
		value="<%=orderPerVRENumber_cfg%>" />
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>