<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@include file="../init.jsp"%>
<%@ page import="org.gcube.portlets.user.vreyard.VreYard" %>
<%@ page import="org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.DetachedREs"%>
<%@ page import="org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.Gateway"%>
<%@ page import="org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VO"%>
<%@ page import="org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE"%>

<portlet:defineObjects />

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(function() {
		$('.vreInfo').tooltip();
	});
</script>
<p class="lead" style="font-weight: 400;">Oops ... we couldn't find the page!</p>
<p class="lead">We couldn't find the page you were looking
	for, perhaps you were interested in one of the following Virtual
	Research Environments, that have been hosted on D4Science in the past.</p>
<div class="vre-yard-portlet">
	<%
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfToDisplay = new SimpleDateFormat("dd MMM yyyy");

		DetachedREs detachedREs = (DetachedREs) request.getAttribute("thedetachedREs");
		for (Gateway gateway : detachedREs.getGateways().values()) {
			List<VRE> gatewayVREs = new ArrayList<VRE>();
			for (VO vo : gateway.getVos().values()) {
				for (VRE vre : vo.getVres().values()) {
					Date date = sdf.parse(vre.getStartDate());
					vre.setStartDate(sdfToDisplay.format(date));
					date = sdf.parse(vre.getEndDate());
					vre.setEndDate(sdfToDisplay.format(date));
					gatewayVREs.add(vre);
				}
			} // end for VO
			pageContext.setAttribute("gatewayVREs", gatewayVREs);
					
	String gatewayDisplayName = gateway.getName().replace("Detached ", ""); 
	String gatewayURL = VreYard.getGatewayHTTPURLByName(gatewayDisplayName);
	if (gatewayURL != null) {
	%>
	<h1><a target="_blank" href="<%=gatewayURL%>"><%=gatewayDisplayName%></a><br> <small><%=gateway.getDescription()%></small>
	<%} 
	else {%>
	<h1><%=gatewayDisplayName%><br> <small><%=gateway.getDescription()%></small>
	<%} %>
	</h1>
	<div style="width: 100%; text-align: left;">
		<table>
			<thead>
				<th class="span3">VRE name</th>
				<th class="span2">Operational from</th>
				<th class="span2">Operational to</th>
				<th class="span2">Managers</th>
			</thead>
			<tbody>
				<c:forEach var="vre" items="${gatewayVREs}">
					<tr style="border-bottom: 1px solid #CCC;">
						<td><button title="${vre.description}" type="button"
								class="btn btn-link vreInfo" data-placement='bottom'>
								${vre.name}</button></td>
						<td>${vre.startDate}</td>
						<td>${vre.endDate}</td>
						<td>${vre.managers}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<%
		} //end for Gateway
	%>
	<div style="width: 100%; text-align: center; margin-top: 75px; margin-bottom: 75px;">
		<h3>Could not find what you are looking for? 
		<p>
			<button class="btn btn-large btn-primary"
				onclick="location.href='https://support.d4science.org'"
				type="button">Visit D4Science Support page</button> or 
			<button class="btn btn-large btn-primary"
				onclick="location.href='https://services.d4science.org/thematic-gateways'"
				type="button">Visit the active Gateways</button></p></h3>
	</div>
</div>