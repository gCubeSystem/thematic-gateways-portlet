package org.gcube.portlets.user.vreyard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.DetachedREsClient;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.DetachedREs;
import org.gcube.portlets.user.thematicgateways.Gateway;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.util.ManagementUtils;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.LayoutSet;
import com.liferay.portal.model.VirtualHost;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutSetLocalServiceUtil;
import com.liferay.portal.service.VirtualHostLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class VreYard
 */
public class VreYard extends MVCPortlet {
	private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil.getLog(VreYard.class);

	//these props are read from portal.ext.properties file
	public static final String DEFAULT_USER_PROPERTY = "d4science.publicportlets.user";
	public static final String DEFAULT_CONTEXT_PROPERTY = "d4science.publicportlets.context";
	public static final String DEFAULT_TOKEN_PROPERTY = "d4science.publicportlets.token";

	public static final String DEFAULT_ROLE = "OrganizationMember";

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {

		try {

			String context = PrefsPropsUtil.getString(PortalUtil.getCompanyId(renderRequest), DEFAULT_CONTEXT_PROPERTY);
			String token = PrefsPropsUtil.getString(PortalUtil.getCompanyId(renderRequest), DEFAULT_TOKEN_PROPERTY);	

			ScopeProvider.instance.set(context);
			SecurityTokenProvider.instance.set(token);
			DetachedREsClient detachedREsClient = new DetachedREsClient();
			DetachedREs detachedREs = detachedREsClient.getDetachedREs();
			renderRequest.setAttribute("thedetachedREs", detachedREs);

		} catch (Exception e) {
			_log.error(e.getLocalizedMessage(), e);

		}


		//renderRequest.setAttribute("theGateways", theGateways);
		super.render(renderRequest, renderResponse);		

	}

	/**
	 * @param gatewayName e.g.DESIRA Gateway
	 * @return the url of the gateway that matches the name, or null there is no match
	 * @throws Exception
	 */
	public static String getGatewayHTTPURLByName(String gatewayName) throws Exception  {
		List<Gateway> theGateways = getGateways(new LiferayGroupManager()); 
		List<VirtualHost> vHosts = VirtualHostLocalServiceUtil.getVirtualHosts(0, VirtualHostLocalServiceUtil.getVirtualHostsCount());
		for (Gateway gateway : theGateways) {
			if (gateway.getSite().getName().compareToIgnoreCase(gatewayName) == 0) {
				LayoutSet lSet = LayoutSetLocalServiceUtil.getLayoutSet(gateway.getSite().getGroupId(), false);
				for (VirtualHost vHost : vHosts) {
					long layoutSetId = vHost.getLayoutSetId();
					if (lSet.getLayoutSetId() == layoutSetId) {
						return "https://"+vHost.getHostname()+"/";
					}
				}
			}
		}
		return null;
	}

	public static List<Gateway> getGateways(GroupManager groupsManager) {
		List<Gateway> toReturn = new ArrayList<>();
		try{
			List<Group> candidateGateways = GroupLocalServiceUtil.getGroups(ManagementUtils.getCompany().getCompanyId(), 0, true);		
			// real gateways have no children as well
			for (Group group : candidateGateways) {
				List<Group> children = group.getChildren(true);
				if(children == null || children.isEmpty())
					if(! (group.getFriendlyURL().equals("/guest") || group.getFriendlyURL().equals("/global") )) {// skipping these sites
						ArrayList<String> theVRENames = new ArrayList<>();
						toReturn.add(new Gateway(group, theVRENames));
					}
			}
		} catch(Exception e){
			_log.error("Failed to retrieve the list of gateways", e);
			return null;
		}
		return toReturn;
	}


}
