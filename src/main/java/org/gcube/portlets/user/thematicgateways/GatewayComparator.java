package org.gcube.portlets.user.thematicgateways;

import java.util.Comparator;

	public class GatewayComparator implements Comparator<Gateway> {
		@Override
		public int compare(Gateway o1, Gateway o2) {
			if (o1.getVres().size() > o2.getVres().size())
				return -1;
			if (o1.getVres().size() < o2.getVres().size())
				return 1;
			return 0;
		}
	}


