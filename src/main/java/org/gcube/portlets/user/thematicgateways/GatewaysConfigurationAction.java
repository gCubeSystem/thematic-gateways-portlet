package org.gcube.portlets.user.thematicgateways;


import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

public class GatewaysConfigurationAction extends DefaultConfigurationAction {
	private static Log _log = LogFactoryUtil.getLog(GatewaysConfigurationAction.class);
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {      	  
		super.processAction(portletConfig, actionRequest, actionResponse);
		PortletPreferences prefs = actionRequest.getPreferences();
		
		String thematicGatewayIntro = prefs.getValue("thematicGatewayIntro", "");     
		String sitesIdToExclude = prefs.getValue("sitesIdToExclude", "");     
		String orderPerVRENumber = prefs.getValue("orderPerVRENumber","false");
		
		_log.info("GatewaysConfigurationAction.processAction() saved correctly");
	
	}

	@Override
	public String render(PortletConfig portletConfig,
			RenderRequest renderRequest, RenderResponse renderResponse)
					throws Exception {
		return "/html/thematic-gateways-portlet/config.jsp";
	}

}