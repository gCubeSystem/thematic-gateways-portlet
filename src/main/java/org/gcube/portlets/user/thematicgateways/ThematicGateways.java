package org.gcube.portlets.user.thematicgateways;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.VirtualGroup;
import org.gcube.vomanagement.usermanagement.util.ManagementUtils;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ThematicGateways
 */
public class ThematicGateways extends MVCPortlet {
	private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil.getLog(ThematicGateways.class);
	GroupManager groupsManager;
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		groupsManager = new LiferayGroupManager();
		List<Gateway> theGateways = getGateways(groupsManager);
		renderRequest.setAttribute("theGateways", theGateways);
		super.render(renderRequest, renderResponse);		

	}


	public List<Gateway> getGateways(GroupManager groupsManager) {
		List<Gateway> toReturn = new ArrayList<>();
		try{
			List<Group> candidateGateways = GroupLocalServiceUtil.getGroups(ManagementUtils.getCompany().getCompanyId(), 0, true);		
			// real gateways have no children as well
			for (Group group : candidateGateways) {
				List<Group> children = group.getChildren(true);
				if(children == null || children.isEmpty())
					if(! (group.getFriendlyURL().equals("/guest") || group.getFriendlyURL().equals("/global") )) {// skipping these sites
						ArrayList<String> theVRENames = new ArrayList<>();
						LinkedHashMap<VRECategory, ArrayList<VRE>> sites = getPortalSitesMappedToVRE(group.getGroupId());
						for (VRECategory cat : sites.keySet()) {
							for (VRE vre : sites.get(cat)) {
								theVRENames.add(vre.getName());
							}
						}
						toReturn.add(new Gateway(group, theVRENames));
						_log.debug("Gateway " + group.getName() + " has " + theVRENames.size() + " VREs");
					}
			}
		} catch(Exception e){
			_log.error("Failed to retrieve the list of gateways", e);
			return null;
		}
		return toReturn;
	}
	

	/**
	 *
	 * @return the Virtual groups with their VREs in the order estabilished in
	 *         the LR Control Panel
	 */
	private LinkedHashMap<VRECategory, ArrayList<VRE>> getPortalSitesMappedToVRE(long currentSiteGroupId) throws Exception {

		LinkedHashMap<VRECategory, ArrayList<VRE>> toReturn = new LinkedHashMap<VRECategory, ArrayList<VRE>>();

		List<VirtualGroup> currentSiteVGroups = groupsManager.getVirtualGroups(currentSiteGroupId);

		for (VirtualGroup vg : currentSiteVGroups) {
			ArrayList<VRE> toCreate = new ArrayList<VRE>();
			VRECategory cat = new VRECategory(1L, vg.getName(), vg.getDescription());
			toReturn.put(cat, toCreate);
		}

		GCubeGroup rootGroupVO = groupsManager.getRootVO();

		try {
			_log.debug("root: " + rootGroupVO.getGroupName());
		} catch (NullPointerException e) {
			_log.error(
					"Cannot find root organziation, please check gcube-data.properties file in $CATALINA_HOME/conf folder, unless your installing the Bundle");
			return toReturn;
		}

		// for each root sub organizations (VO)
		for (GCubeGroup vOrg : rootGroupVO.getChildren()) {
			for (GCubeGroup vreSite : vOrg.getChildren()) {
				long vreID = vreSite.getGroupId();
				String vreName = vreSite.getGroupName();
				String vreDescription = vreSite.getDescription();
				List<VirtualGroup> vreGroups = groupsManager.getVirtualGroups(vreID);
				for (VirtualGroup vreGroup : vreGroups) {
					for (VRECategory vre : toReturn.keySet()) {
						if (vre.getName().compareTo(vreGroup.getName()) == 0) {
							ArrayList<VRE> toUpdate = toReturn.get(vre);
							VRE toAdd = new VRE(vreName, vreDescription, vreID, "", "");
							toUpdate.add(toAdd);
						}
					}
				}
			}
		}
		return toReturn;
	}

	
	private class VRECategory {
		private long categoryID;
		private String name;
		private String description;

		public VRECategory(long categoryID, String name, String description) {
			super();
			this.categoryID = categoryID;
			this.name = name;
			this.description = description;
		}

		@SuppressWarnings("unused")
		public long getCategoryID() {
			return categoryID;
		}

		@SuppressWarnings("unused")
		public String getDescription() {
			return description;
		}

		public String getName() {
			return name;
		}

		@Override
		public String toString() {
			return "VRECategory [categoryID=" + categoryID + ", name=" + name + ", description=" + description + "]";
		}

	}
	private class VRE implements Comparable<VRE> {
		private String name;
		private String description;
		private long id;
		private String url;
		private String scope;

		public VRE(String name, String description, long id, String url, String scope) {
			super();
			this.name = name;
			this.description = description;
			this.id = id;
			this.url = url;
			this.scope = scope;
		}

		@SuppressWarnings("unused")
		public String getDescription() {
			return description;
		}

		@SuppressWarnings("unused")
		public long getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		@SuppressWarnings("unused")
		public String getScope() {
			return scope;
		}

		@SuppressWarnings("unused")
		public String getUrl() {
			return url;
		}

		@Override
		public String toString() {
			return "VRE [name=" + name + ", description=" + description + ", id=" + id + ", url=" + url + ", scope="
					+ scope + "]";
		}

		@Override
		public int compareTo(VRE vre) {
			return this.getName().compareTo(vre.getName());
		}
	}

}