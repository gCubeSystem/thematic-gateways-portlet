package org.gube.portlets.user.kpidata;

public enum PFISHGrowingStages {
	
	GROW_OUT_INDIVIDUAL("Grow out Individual"),
	GROW_OUT_INDIVIDUAL_CLOSED_BATCHES("Grow out Individual (closed)"),
	GROW_OUT_AGGREGATED("Grow out Aggregated"),
	GROW_OUT_AGGREGATED_CLOSED_BATCHES("Grow out Aggregated (closed)"),
	HATCHERY_INDIVIDUAL("Hatchery Individual"),
	HATCHERY_INDIVIDUAL_CLOSED_BATCHES("Hatchery Individual (closed)"),
	PRE_ONGROWING("Pre-grow Individual"),
	PRE_ONGROWING_CLOSED_BATCHES("Pre-grow Individual (closed)");

	private String filename;
	 
	PFISHGrowingStages(String filename) {
        this.filename = filename;
    }
 
    public String getFileName() {
        return filename;
    }
}
