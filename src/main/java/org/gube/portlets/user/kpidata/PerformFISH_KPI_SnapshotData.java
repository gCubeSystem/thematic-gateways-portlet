package org.gube.portlets.user.kpidata;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.io.IOUtils;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class PerformFISH_KPI_SnapshotData
 */
public class PerformFISH_KPI_SnapshotData extends MVCPortlet {
	private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil.getLog(PerformFISH_KPI_SnapshotData.class);
	private static final String KPI_DATA_FOLDER_NAME = "KPI-Data";


	public static final String PERFORM_SERVICE_METHOD_ENDPOINT = "/performance?gcube-token=";

	public void render(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		super.render(request, response);
	}

	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String token = "";
		long groupId = 0;
		String context = "";
		String username = "";
		try {
			Group group = (Group) GroupLocalServiceUtil.getGroup(PortalUtil.getScopeGroupId(resourceRequest));
			groupId = group.getGroupId();
			context = KPIdataUtils.getCurrentContext(groupId);
			username = PortalUtil.getUser(resourceRequest).getScreenName();
			token = PortalContext.getConfiguration().getCurrentUserToken(context, username);
		} catch (PortalException | SystemException e1) {
			e1.printStackTrace();
		}
		resourceResponse.setContentType("application/json");

		String endpoint = KPIdataUtils.getAnalyticalToolkitEndpoint(context);
		String idFolderWhereToWriteFiles = createFoldersIfNotExists(token, context);
		StringBuilder sb = new StringBuilder(endpoint)
				.append(PERFORM_SERVICE_METHOD_ENDPOINT)
				.append(token)
				.append("&batch_type=");
		_log.info("calling performService on context " + context);
		List<CallResult> results = null;
		try {
			results = performAllCalls(sb.toString(), token, context, idFolderWhereToWriteFiles);
		} catch (JSONException | IOException e) {
			e.printStackTrace();
		}

		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		for (CallResult result : results) {
			JSONObject fileObject = JSONFactoryUtil.createJSONObject();
			fileObject.put("success", result.isSuccess());
			fileObject.put("comment", result.getComment());
			jsonArray.put(fileObject);
		}
		resourceResponse.getWriter().println(jsonArray);	
		super.serveResource(resourceRequest, resourceResponse);

	}
	/**
	 * 
	 * @param endpoint
	 * @param authorizationToken
	 * @param context
	 * @param folderOfTheDayId
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 */
	private List<CallResult> performAllCalls(String endpoint, String authorizationToken, String context, String folderOfTheDayId)  throws JSONException, IOException {
		List<CallResult> callResults = new ArrayList<>();
		for(PFISHGrowingStages type : PFISHGrowingStages.values())	{
			_log.info(type + "-" + type.getFileName());
			String request = endpoint+type;
			JSONObject jsonObj = readFromURL(request);

			ScopeProvider.instance.set(context);
			SecurityTokenProvider.instance.set(authorizationToken);
			StorageHubClient shc = new StorageHubClient();

			Iterator<String> iterator = jsonObj.keys();
			iterator.forEachRemaining((key) -> {
				if (!key.endsWith("_internal")) {
					StringBuilder sb = new StringBuilder(type.getFileName()).append(" - ").append(key).append(".csv");
					String httpURLofTheCSV = jsonObj.getString(key);
					boolean result = saveToWorkspaceVREFolder(shc, sb.toString(), httpURLofTheCSV, folderOfTheDayId);
					callResults.add(new CallResult(result, sb.toString()));
				}
			});

		}
		return callResults;
	}

	/**
	 * 
	 * @param filename
	 * @param fileURL
	 * @param authorizationToken
	 * @param context
	 * @param folderOfTheDayId
	 * @return
	 */
	private boolean saveToWorkspaceVREFolder(StorageHubClient shc, String filename, String fileURL, String folderOfTheDayId) {
		try {
			FolderContainer folderOfTheDay = shc.open(folderOfTheDayId).asFolder();
			InputStream is = new URL(fileURL).openStream();
			folderOfTheDay.uploadFile(is, filename, "");
			_log.info("uploaded file " + filename + " with success");
		} catch (Exception e) {
			_log.error("Failed to upload file " + filename, e);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param authorizationToken
	 * @param context
	 * @return the is of the folder for the day, it creates it of not existent
	 */
	private String createFoldersIfNotExists(String authorizationToken, String context) {
		String idFolderWhereToWriteFiles = null;
		ScopeProvider.instance.set(context);
		SecurityTokenProvider.instance.set(authorizationToken);
		StorageHubClient shc = new StorageHubClient();
		FolderContainer vreFolder = shc.openVREFolder();
		try {
			List<? extends Item> items = vreFolder.list().getItems();
			FolderContainer kpiDataFolder = null;
			//verify that KPI_DATA_FOLDER_NAME folder exists, else create it
			boolean kpiDataFolderExists = false;
			for (Item item :  items) {
				if (item instanceof FolderItem && item.getName().equalsIgnoreCase(KPI_DATA_FOLDER_NAME)) {
					kpiDataFolderExists = true;
					kpiDataFolder = shc.open(item.getId()).asFolder();
					break;
				}
			}
			if (!kpiDataFolderExists)  
				kpiDataFolder = vreFolder.newFolder(KPI_DATA_FOLDER_NAME, "contains the generated data collected and anonymised");
			//e.g. 2021-01-22
			String folderOfTheDay = computeDayFoldername();
			boolean folderOfTheDayExists = false;
			for (Item item : kpiDataFolder.list().getItems()) {
				if (item instanceof FolderItem && item.getName().equalsIgnoreCase(folderOfTheDay)) {
					folderOfTheDayExists = true;
					idFolderWhereToWriteFiles = item.getId();
				}
			}
			if (!folderOfTheDayExists) {
				idFolderWhereToWriteFiles = kpiDataFolder.newFolder(folderOfTheDay, "contains the generated data collected and anonymised for the day " + folderOfTheDay).getId();	
				_log.info("Created the folder for the day (" + folderOfTheDay + ") into KPI-Data Folder, returning id="+idFolderWhereToWriteFiles);
			} else {
				_log.info("The folder for the day (" + folderOfTheDay + ") exists already into KPI-Data Folder, returning id="+idFolderWhereToWriteFiles);
			}
		} catch (StorageHubException e) {
			e.printStackTrace();
			return null;
		}
		return idFolderWhereToWriteFiles;
	}



	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	private static JSONObject readFromURL(String request) throws IOException, JSONException {		
		URL url= new URL(null, request);
		HttpsURLConnection conn= (HttpsURLConnection) url.openConnection();           
		conn.setDoOutput( true );
		conn.setInstanceFollowRedirects( false );
		conn.setRequestMethod( "GET" );
		conn.setRequestProperty( "Content-Type", "application/json"); 
		conn.setRequestProperty( "charset", "utf-8");
		conn.setUseCaches( false );

		InputStream in = conn.getInputStream();
		String encoding = conn.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String body = IOUtils.toString(in, encoding);

		JSONObject jsonObj =  JSONFactoryUtil.createJSONObject(body);

		return jsonObj;
	}

	//this returns the date as String as we want it: e.g. 2021-01-22
	private String computeDayFoldername() {
		String format = "yyyy-MM-dd";
		DateFormat dateFormatter = new SimpleDateFormat(format);
		String date = dateFormatter.format(new Date());
		return date;
	}
}
