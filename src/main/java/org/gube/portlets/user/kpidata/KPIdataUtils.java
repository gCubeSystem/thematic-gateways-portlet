package org.gube.portlets.user.kpidata;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.net.URI;
import java.util.Collection;
import java.util.List;

import org.gcube.common.portal.PortalContext;
import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.GCoreEndpoint.Profile.Endpoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class KPIdataUtils {
	private static Log _log = LogFactoryUtil.getLog(KPIdataUtils.class);
	
	private static String ANALYTICAL_TOOLKIT_SERVICE_GCORE_ENDPOINT_NAME = "perform-service";
	private static String ANALYTICAL_TOOLKIT_SERVICE_GCORE_ENDPOINT_CLASS = "Application";
	public static String ANALYTICAL_TOOLKIT_SERVICE_INTERFACE_NAME = "org.gcube.application.perform.service.PerformService";

	public KPIdataUtils() {
		// TODO Auto-generated constructor stub
	}
	public static String getCurrentContext(long groupId) {
		try {
			PortalContext pContext = PortalContext.getConfiguration(); 
			return pContext.getCurrentScope(""+groupId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<GCoreEndpoint> getAnalyticalToolkitServiceInstance(String context) throws Exception  {
		String currScope = 	ScopeProvider.instance.get();
		ScopeProvider.instance.set(context);
		SimpleQuery query = queryFor(GCoreEndpoint.class);
		query.addCondition("$resource/Profile/ServiceClass/text() eq '"+ ANALYTICAL_TOOLKIT_SERVICE_GCORE_ENDPOINT_CLASS +"'");
		query.addCondition("$resource/Profile/ServiceName/text() eq '"+ ANALYTICAL_TOOLKIT_SERVICE_GCORE_ENDPOINT_NAME +"'");
		DiscoveryClient<GCoreEndpoint> client = clientFor(GCoreEndpoint.class);
		List<GCoreEndpoint> toReturn = client.submit(query);
		ScopeProvider.instance.set(currScope);
		return toReturn;
	}	
	
	public static String getAnalyticalToolkitEndpoint(String context) {
		List<GCoreEndpoint> analyticalServices = null;
		try {
			analyticalServices = getAnalyticalToolkitServiceInstance(context);
			if (analyticalServices == null || analyticalServices.isEmpty()) {
				return "Cound not find Analytical Toolkit service";
			}	
			GCoreEndpoint endpoint = analyticalServices.get(0);
			Collection<Endpoint> list = endpoint.profile().endpoints().asCollection();

			URI theURI = null;
			for (Endpoint ep : list) {
				if (ep.name().equals(ANALYTICAL_TOOLKIT_SERVICE_INTERFACE_NAME)) {
					_log.info("Analytical Toolkit GCoreEndpoint: "+ep.uri());
					theURI = ep.uri();
				}
			}
			String endpointSSL = "https://"+theURI.getHost()+theURI.getPath();
			_log.info("Returning endpointSSL " + endpointSSL);
			return endpointSSL;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
