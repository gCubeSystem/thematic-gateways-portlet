# Changelog

## [v7.3.0]

- maven-portal-bom 4.0.0
- maven-parent 1.2.0

## [v7.2.0-SNAPSHOT] - 2021-01-22

- Added portlet for PerformFISH KPI-Data generation

## [v7.1.0] - 2020-10-09

### Features

- Added the meta-landing page aiming at avoiding broken links page [#19868]

## [v1.0.0] - 2019-10-01

First release

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)